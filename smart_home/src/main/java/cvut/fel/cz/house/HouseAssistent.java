package cvut.fel.cz.house;

import cvut.fel.cz.actions.ReportBuilder;
import cvut.fel.cz.actors.Adult;

import cvut.fel.cz.actors.Child;
import cvut.fel.cz.devices.Heater;
import cvut.fel.cz.documentation.HouseConfigurationReport;

import javax.swing.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Třída {@code HouseAssistent} představuje asistenta domácnosti, který provádí akce v domě.
 * Zajišťuje kontrolu času, teploty, oken a generuje zprávy a vyúčtování.
 */
public class HouseAssistent {

    /**
     * Instance domácího asistenta, implementovaná jako Singleton.
     */
    private static HouseAssistent houseAssistent;

    /**
     * Aktuální datum v simulaci domácnosti.
     */
    private static LocalDate date;

    /**
     * Noční čas, kdy se nastavují žaluzie na noční režim.
     */
    private static LocalTime nightTime;

    /**
     * Denní čas, kdy se nastavují žaluzie na denní režim.
     */
    private static LocalTime dayTime;

    /**
     * Aktuální čas v simulaci domácnosti.
     */
    private static LocalTime time;

    /**
     * Odkaz na objekt domu, který spravuje asistent.
     */
    private final House house;

    /**
     * Informace, zda právě prší.
     */
    private static boolean raining;

    /**
     * Soukromý konstruktor pro vytvoření instance domácího asistenta.
     *
     * @param house Objekt domu, který asistent spravuje.
     */
    private HouseAssistent(House house) {
        this.house = house;
        nightTime = LocalTime.of(22, 0);
        dayTime = LocalTime.of(8, 0);
        time = LocalTime.of(7, 0);
        date = LocalDate.of(2024, 1, 1);
        Bills.createBillSession();
        generateConfigurationReport();
        raining = false;
    }
    /**
     * Vrátí instanci domácího asistenta (Singleton).
     *
     * @param house Objekt domu, který asistent spravuje.
     * @return Instance domácího asistenta.
     */
    public static HouseAssistent getInstance(House house) {
        if (houseAssistent == null) {
            houseAssistent = new HouseAssistent(house);
        }
        return houseAssistent;
    }


    /**
     * Přidá jednu hodinu k aktuálnímu času a zkontroluje prováděné akce.
     */
    public void addHour() {
        time = time.plusHours(1);
        checkTime();
    }

    /**
     * Přidá zadaný počet minut k aktuálnímu času a zkontroluje prováděné akce.
     *
     * @param minutes Počet minut k přidání.
     */
    public void addMinutes(int minutes) {
        time = time.plusMinutes(minutes);
        checkTime();
    }

    /**
     * Přidá půlhodinu k aktuálnímu času a zkontroluje prováděné akce.
     */
    public void addHalfHour() {
        time = time.plusMinutes(30);
        checkTime();
        checkHeaters();
    }

    /**
     * Zkontroluje a aktualizuje teplotu v místnostech se zapnutým topením.
     */
    public void checkHeaters() {
        List<Room> rooms = house.getFloors().stream()
                .flatMap(floor -> floor.getRooms().stream()).toList();
        for (Room room : rooms) {
            Heater heater = room.getHeater();
            if (heater.isTurnedOn()) {
                heater.setActualTemperature(heater.getActualTemperature() + 0.5);
            } else {
                heater.setActualTemperature(heater.getActualTemperature() - 0.5);
            }
            System.out.println("Temperature in: " + room.getNameOfRoom() + " is " + heater.getActualTemperature());
            if (heater.getActualTemperature() >= heater.getWantedTemperature()) {
                heater.turnOf();
                System.out.println("Heater was turned off in " + room.getNameOfRoom() + "\n");
            } else {
                heater.turnOn();
                System.out.println("Heater was turned on in " + room.getNameOfRoom() + "\n");
            }
        }
    }

    /**
     * Zkontroluje aktuální čas a provede příslušné akce (např. změnu režimu žaluzií).
     */
    private void checkTime() {
        if (time.isAfter(nightTime)) {
            setToNightMode();
        } else if (time.isAfter(dayTime.minusMinutes(20)) && time.isBefore(dayTime.plusMinutes(20))) {
            setToDayMode();
        } else if (time.isBefore(LocalTime.of(0, 10))) {
            date = date.plusDays(1);
            checkBillingSession();
            System.out.println("New day begins");
        }
    }

    /**
     * Zobrazí informace o aktuálním stavu obyvatel domu.
     */
    public void showInfo() {
        StringBuilder str = new StringBuilder();
        str.append("At ").append(time.toString()).append(": ").append('\n');
        for (Child occupant : house.getChildren()) {
            if (occupant.getOccupiingRoom() == null) {
                str.append(occupant.getName()).append(" was outside").append('\n');
            } else {
                str.append(occupant.getName()).append(" was in: ").append(occupant.getOccupiingRoom().getNameOfRoom()).append('\n');
            }
        }
        for (Adult occupant : house.getAdults()) {
            if (occupant.getOccupiingRoom() == null) {
                str.append(occupant.getName()).append(" was outside").append('\n');
            } else {
                str.append(occupant.getName()).append(" was in: ").append(occupant.getOccupiingRoom().getNameOfRoom()).append('\n');
            }
        }
        System.out.println(str.toString());
    }

    /**
     * Nastaví žaluzie na noční režim.
     */
    private void setToNightMode() {
        System.out.println("Home assistant is setting blinds to night mode");
        System.out.println(" ");
        List<Window> allWindows = house.getFloors().stream()
                .flatMap(floor -> floor.getRooms().stream())
                .flatMap(room -> room.getWindows().stream())
                .toList();
        for (Window window : allWindows) {
            window.closeBlinds();
        }
    }

    /**
     * Nastaví žaluzie na denní režim.
     */
    private void setToDayMode(){
        System.out.println("Home assistant is setting blinds to day mode");
        System.out.println(" ");
        List<Window> allWindows = house.getFloors().stream()
                .flatMap(floor -> floor.getRooms().stream())
                .flatMap(room -> room.getWindows().stream())
                .toList();
        for (Window window : allWindows) {
            window.openBlinds();
        }
    }



    /**
     * Zapíná a vypíná simulaci deště.
     */
    public void switchRainning(){
        raining=!raining;
        checkWindows();
    }


    /**
     * Při dešti zavře všechna okna.
     */
    private void checkWindows(){
        if(raining){
            System.out.println("It started raining");
            List<Window> allWindows = this.house.getFloors().stream()
                    .flatMap(floor -> floor.getRooms().stream())
                    .flatMap(room -> room.getWindows().stream())
                    .toList();

            for(Window window:allWindows){
                window.closeWindow();
                System.out.println("Window was closed by Home assistant");
            }
        }
    }


    /**
     * Při změně dne zkontroluje jestli už neuplynuj měsíc a neni tak čas vytvořit nové vyučtování.
     */
    private void checkBillingSession(){
        if(date.isAfter(date.plusMonths(1).minusDays(1))){
            ReportBuilder.buildReport("Consumption");
            Bills.createBillSession();
            System.out.println("House assistant generates new billing session");
        }
    }


    /**
     * Vytvoří report o struktuře domu.
     */
    private void generateConfigurationReport(){
        HouseConfigurationReport report=new HouseConfigurationReport(house,"HouseConfiguration");
        report.acquireData();
        report.createReportText();
    }

    public static LocalTime getTime() {
        return time;
    }

    public static LocalDate getDate() {
        return date;
    }

    public static LocalTime getNightTime() {
        return nightTime;
    }

    public static LocalTime getDayTime() {
        return dayTime;
    }

    public boolean isRaining() {
        return raining;
    }

    public void setRaining(boolean raining) {
        this.raining = raining;
    }
}
