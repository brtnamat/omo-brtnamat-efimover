package cvut.fel.cz.house;

import cvut.fel.cz.actors.Person;
import cvut.fel.cz.devices.ElectricDevice;
import cvut.fel.cz.devices.Heater;
import cvut.fel.cz.devices.Lights;

import java.util.ArrayList;
import java.util.List;

public class Room {
    private String nameOfRoom;
    private List<Window> windows;
    private List<Person> peopleInRoom;
    private Lights lights;
    protected Floor floor;

    private List<ElectricDevice> devices;

    private Heater heater;


    public Room(String nameOfRoom) {
        this.nameOfRoom = nameOfRoom;
//        this.floor=floor;
        this.windows = new ArrayList<>();
        this.lights =  new Lights();
        this.peopleInRoom=new ArrayList<Person>();
        this.heater=new Heater(22);
        this.devices=new ArrayList<>();
    }
    public List<Window> getWindows() {
        return windows;
    }

    public String getNameOfRoom() {
        return nameOfRoom;
    }

    public Lights getLights() {
        return lights;
    }

    public List<ElectricDevice> getDevices() {
        return devices;
    }
    public void addToDevices(ElectricDevice device){
        this.devices.add(device);

    }

    public Heater getHeater() {
        return heater;
    }

    public void setWindows(List<Window> windows) {
        this.windows = windows;
    }
}
