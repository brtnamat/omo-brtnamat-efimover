package cvut.fel.cz.house;

import cvut.fel.cz.actions.Event;
import cvut.fel.cz.actors.EventHandler;
import cvut.fel.cz.devices.Sensor;

import java.util.List;

public class Window extends EventHandler{
    protected Blinds blinds;
    protected Sensor openSensor;

    public Window(List<? extends EventHandler> subscribers) {
        super("Window", (List<EventHandler>) subscribers);
        this.blinds = new Blinds();
        this.openSensor=new Sensor((List<EventHandler>) subscribers);
    }

    public Blinds getBlinds() {
        return blinds;
    }
    public void openWindow() {
        openSensor.turnOn();
        this.notifySubscribers("was opened");
    }
    public void closeWindow(){
        openSensor.turnOff();
        this.notifySubscribers("was closed");
    }

    public void openBlinds(){
        blinds.setClosed(false);
        System.out.println("Blind were opened");
    }
    public void closeBlinds(){
        blinds.setClosed(true);
        System.out.println("Blinds were closed");
    }

}
