package cvut.fel.cz.house;

import cvut.fel.cz.actors.Person;

import java.util.ArrayList;
import java.util.List;

public class Floor {
    protected int numberOfFloor;
    private List<Room> rooms;

    public Floor(int numberOfFloor) {
        this.numberOfFloor = numberOfFloor;
        this.rooms = rooms;
        this.rooms=new ArrayList<>();
    }

    public int getNumberOfFloor() {
        return numberOfFloor;
    }

    public List<Room> getRooms() {
        return rooms;
    }
}
