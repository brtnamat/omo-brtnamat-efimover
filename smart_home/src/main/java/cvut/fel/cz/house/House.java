package cvut.fel.cz.house;

import cvut.fel.cz.actors.Adult;
import cvut.fel.cz.actors.Child;
import cvut.fel.cz.actors.Person;
import cvut.fel.cz.actors.Pet;
import cvut.fel.cz.devices.Car;
import cvut.fel.cz.devices.ElectricDevice;

import javax.swing.plaf.ListUI;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class House {
    private List<Floor> floors;
    private List<Adult> adults;
    private List<Child> children;
    private List<Pet> pets;
    private static Room garage;
    private static Room entrance;
    private List<ElectricDevice> devices;
    private static House house;
    private HouseAssistent houseAssistent;

    private Car car;
    private Bills bills;
    private Date date;

    private House(List<Floor> floors, List<Child> children,List<Adult> adults, List<Pet> pets) {
        this.floors = floors;
        this.children=children;
        this.adults=adults;
        this.pets=pets;
        this.houseAssistent=HouseAssistent.getInstance(this);
        this.devices=new ArrayList<>();
        entrance=floors.get(0).getRooms().get(0);
    }
    public static House getInstance (List<Floor> floors, List<Child> children, List<Adult> adults, List<Pet> pets){
        if(house==null){
            house=new House(floors, children,adults,pets);
        }
        return house;
    }

    public List<Floor> getFloors() {
        return floors;
    }

    public List<Adult> getAdults() {
        return adults;
    }

    public List<Child> getChildren() {
        return children;
    }

    public List<Pet> getPets() {
        return pets;
    }

    public HouseAssistent getHouseAssistent() {
        return houseAssistent;
    }

    public void addToDevices(ElectricDevice device){
        this.devices.add(device);
    }

    public List<ElectricDevice> getDevices() {
        return devices;
    }

    public void setDevices(List<ElectricDevice> devices) {
        this.devices = devices;
    }

    public Car getCar() {
        return car;
    }
    public void addCar(Car car){
        if (this.car==null){
            this.car=car;
        }
    }

    public static Room getGarage() {
        return garage;
    }

    public static void setGarage(Room garage) {
        House.garage = garage;
    }

    public static Room getEntrance() {
        return entrance;
    }
}
