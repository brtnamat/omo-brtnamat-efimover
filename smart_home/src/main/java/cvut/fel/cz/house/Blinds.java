package cvut.fel.cz.house;

public class Blinds {

    protected boolean closed;

    public boolean isClosed() {
        return closed;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }
}
