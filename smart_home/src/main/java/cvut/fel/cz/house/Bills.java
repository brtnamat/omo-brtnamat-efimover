package cvut.fel.cz.house;

import java.time.LocalDate;
import java.util.Date;

public class Bills {
    private static final double electricityCost = 0.284; // eur/KWh
    private static final double gasCost = 0.113; // eur/m3
    private static final double waterCost = 3.0 ; // eur/m3

    private static double usedWater;
    private static double usedElectricity;
    private static double usedGas;
    private static LocalDate fromDate;



    public static void createBillSession(){
        usedWater=0;
        usedElectricity=0;
        usedGas=0;
        fromDate=HouseAssistent.getDate();
    }

    public static void addToUsedWater(double amount){
        usedWater=usedWater+amount;
    }

    public static void addToUsedGas(double amount){
        usedGas=usedGas+amount;
    }
    public static void addToUsedElectricity(double amount){
        usedElectricity=usedElectricity+amount;
    }

    public static double getElectricityCost() {
        return electricityCost;
    }

    public static double getGasCost() {
        return gasCost;
    }

    public static double getWaterCost() {
        return waterCost;
    }

    public static double getUsedWater() {
        return usedWater;
    }

    public static double getUsedElectricity() {
        return usedElectricity;
    }

    public static double getUsedGas() {
        return usedGas;
    }

    public static LocalDate getFromDate() {
        return fromDate;
    }

}
