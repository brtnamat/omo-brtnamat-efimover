package cvut.fel.cz.actors;

import cvut.fel.cz.actions.Event;
import cvut.fel.cz.devices.ElectricDevice;
import cvut.fel.cz.devices.DeviceException;
import cvut.fel.cz.house.HouseAssistent;
import cvut.fel.cz.house.Window;

import java.time.LocalTime;
import java.util.ArrayList;
/**
 * Abstraktní třída {@code Person} představuje osobu v simulaci domácnosti.
 * Tato třída je odvozena od třídy {@code LivingBeing}.
 */
public abstract class Person extends LivingBeing{


    /**
     * Zařízení, které aktuálně používá tato osoba.
     */
private ElectricDevice usingDevice;


    /**
     * Konstruktor třídy {@code Person}.
     *
     * @param type       Typ osoby.
     * @param subscribers Seznam odběratelů událostí.
     * @param age        Věk osoby.
     * @param name       Jméno osoby.
     */
public Person(String type, ArrayList<? extends EventHandler> subscribers, int age, String name) {
    super(type, subscribers, age, name);
}



    /**
     * Začne používat určené zařízení v dané místnosti.
     *
     * @param usedDevice Elektrické zařízení, které bude používáno.
     * @throws DeviceException Pokud dojde k chybě při používání zařízení.
     */
public void useDevice(ElectricDevice usedDevice) throws DeviceException {
    try {
        enterRoom(usedDevice.getOccupiedRoom());
        if(usedDevice.getUser()==null){
            usedDevice.use(this);
            this.usingDevice=usedDevice;
            System.out.println(this.getName()+" started using "+usedDevice.getType()+"\n");
        }
    } catch (DeviceException e) {
        throw new DeviceException();
    }

}

/**
 * Přestane používat aktuálně používané elektrické zařízení.
 */
public void stopUsingDevice(){
        if(usingDevice!=null) {
            if (usingDevice.getUser() == this) {
                System.out.println(this.getName() + " stopped using " + this.usingDevice.getType());
                this.usingDevice.stopUsing();
                this.usingDevice = null;
            }
        }
    }






    /**
     * Otevře okno v místnosti.
     *
     * @param window Okno, které bude otevřeno.
     */
    public void openWindow(Window window){
        window.openWindow();
    }

    /**
     * Zavře okno v místnosti.
     *
     * @param window Okno, které bude zavřeno.
     */
    public void closeWindow(Window window) {
        window.closeWindow();
    }

    /**
     * Zavře žaluzie u okna v místnosti.
     *
     * @param window Okno, u kterého budou zavřeny žaluzie.
     */
    public void closeBlinds(Window window) {
        window.closeBlinds();
    }

    /**
     * Otevře žaluzie u okna v místnosti.
     *
     * @param window Okno, u kterého budou otevřeny žaluzie.
     */
    public void openBlinds(Window window) {
        window.openBlinds();
    }
    public ElectricDevice getUsingDevice() {
        return usingDevice;
    }
}
