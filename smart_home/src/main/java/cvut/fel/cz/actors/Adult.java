package cvut.fel.cz.actors;

import cvut.fel.cz.Simulation;
import cvut.fel.cz.actions.Event;
import cvut.fel.cz.devices.Car;
import cvut.fel.cz.devices.DeviceRunningState;
import cvut.fel.cz.devices.ElectricDevice;
import cvut.fel.cz.house.House;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class Adult extends Person{



    private House house;
    public Adult(ArrayList<EventHandler> subscribers, int age, String name) {
        super("Adult",subscribers, age, name);
    }




    public void driveCar(Car car, double kms){
        if(car.isInUse()){
            System.out.println(this.getName()+ " tried to use car that is already in use");
            return;
        }
        car.setInUse(true);
        car.unlock();
        car.start();
        car.drive(this, kms);
        this.leaveHouse();
        this.setStatus("Driving car");
    }



    @Override
    public void handleEvent(Event event){
        Event.removeFromUnhandledEvents(event);
        if(event.getEventText().equals("broke")){
            ElectricDevice ec= (ElectricDevice)event.getCaller();
            ec.getDocumentation().read(this);
            ec.setDurability(100);
            ec.setState(DeviceRunningState.Idle);
            ec.notifySubscribers("was fixed");
            System.out.println(this.name+" fixed "+ ec.getType());
        }else if (event.getEventText().equals("Car will need refueling")){
            Car car=(Car)event.getCaller();
            car.fillTheTank();
            car.notifySubscribers("tank was filled");
            System.out.println(this.name+" filled tank of car");
        }

        System.out.println("Event "+event.getEventText()+" was solved by: "+this.getId());
    }



    public void returnWithCar(Car car){
        if(!Objects.equals(getStatus(), "Driving car")){
            return;
        }
        car.stop();
        car.lock();
        car.setInUse(false);
        this.setStatus("Home");
        this.returnToHouse();
    }
}
