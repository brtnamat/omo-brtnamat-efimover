package cvut.fel.cz.actors;

import cvut.fel.cz.actions.Event;

public interface EventHandlerInterface {

    void notifySubscribers(String eventType);

    void getNotification(String eventType,EventHandler caller);
}
