package cvut.fel.cz.actors;

import cvut.fel.cz.actions.Event;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * Abstraktní třída {@code EventHandler} představuje zařízení/bytost která dokáže obsluhovat a generovat událost.
 * Tato třída implementuje rozhraní {@code EventHandlerInterface}.
 */
public abstract class EventHandler implements EventHandlerInterface {

    /**
     * Typ událostního obslužného.
     */
    protected String type;

    /**
     * Seznam odběratelů událostí.
     */
    protected List<EventHandler> subscribers;

    /**
     * Jedinečný identifikátor událostního obslužného.
     */
    protected UUID id;


    /**
     * Konstruktor třídy {@code EventHandler}.
     *
     * @param type        Typ událostního obslužného.
     * @param subscribers Seznam odběratelů událostí.
     */
    public EventHandler(String type, List<EventHandler> subscribers) {
        this.type = type;
        this.id=UUID.randomUUID();
        this.subscribers = subscribers;
    }



    /**
     * Informuje odběratele o události.
     *
     * @param eventType Typ události.
     */
    public void notifySubscribers(String eventType) {
        if(this.subscribers==null){
            System.out.println("Notification from caller with id: "+getId()+" was accepted by no one");
            return;
        }
        for (EventHandler subscriber : subscribers) {
            subscriber.getNotification(eventType,this);
        }
    }


    /**
     * Přijímá oznámení od jiného událostního Event handleru.
     *
     * @param eventType Typ události.
     * @param caller    Událostní obslužný, který oznámil událost.
     */
    public void getNotification(String eventType,EventHandler caller) {
        Event event=new Event(eventType,caller,this);
        event.logEvent();
        if(Objects.equals(eventType, "broke") || Objects.equals(eventType, "Empty food bowl") || Objects.equals(eventType, "Empty water bowl") ){
            Event.addToUnhandledEvents(event);
        }
    }

    /**
     * Zpracuje událost.
     *
     * @param event Událost k zpracování.
     */
    public void handleEvent(Event event){
        Event.removeFromUnhandledEvents(event);
        System.out.println("Event "+event.getEventText()+" was solved by: "+this.getId());
    }

    /**
     * Zpracuje všechny možné události kde je označen jako handler.
     */
    public void handleAllPossibleEvents(){
        List<Event> eventsToDelete=new ArrayList<>();
        for(Event event: Event.getUnhandledEvents()){
            if(event.getHandler()==this){
                System.out.println("Event "+event.getEventText()+" was solved by: "+this.getId());
                eventsToDelete.add(event);
            }
        }
        for (Event event:eventsToDelete){
            Event.getUnhandledEvents().remove(event);
            Event.getUnhandledEvents().removeIf(event1 -> event.getCaller().getId().equals(event1.getCaller().getId()) && event.getEventText().equals(event1.getEventText()));
            System.out.println("Event solved: "+   event.getCaller().getId()+" was solved by"+event.getHandler().getId());
        }

    }


    /**
     * Přidá instanci nového odběratele.
     *
     * @param subscriber Nový odběratel.
     */
    public void addToSubscribers(EventHandler subscriber){
        if(subscribers==null){
            subscribers=new ArrayList<>();
        }
        this.subscribers.add(subscriber);
    }

    public List<EventHandler> getSubscribers() {
        return subscribers;
    }

    public String getType() {
        return type;
    }

    public UUID getId() {
        return id;
    }
}
