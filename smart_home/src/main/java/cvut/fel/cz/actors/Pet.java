package cvut.fel.cz.actors;

import cvut.fel.cz.actions.Event;
import cvut.fel.cz.devices.Sensor;

import java.util.ArrayList;
import java.util.List;

public class Pet extends LivingBeing{

    private Sensor emptyFoodBowlSensor;

    private Sensor emptyWaterBowlSensor;
    private double foodbowl;
    private double waterbowl;

    public Pet(ArrayList<? extends EventHandler> subscribers, int age, String name) {
        super("Pet", (List<EventHandler>) subscribers, age, name);
        this.emptyFoodBowlSensor=new Sensor((List<EventHandler>) subscribers);
        this.emptyWaterBowlSensor=new Sensor((List<EventHandler>) subscribers);
        this.foodbowl=100;
    }


    public void eat(){
        if(foodbowl<=0){
            return;
        }
        this.foodbowl=foodbowl-20;
        if(foodbowl<=20){
            this.emptyFoodBowlSensor.turnOn();
            this.emptyFoodBowlSensor.notifySubscribers("Empty food bowl");
        }
        System.out.println("Dog consumed food, "+foodbowl+" % left in foodbowl");
        System.out.println(" ");
    }
    public void drink(){
        if(waterbowl<=0){
            return;
        }
        this.waterbowl=waterbowl-20;
        if(waterbowl<=20){
            this.emptyWaterBowlSensor.turnOn();
            this.emptyWaterBowlSensor.notifySubscribers("Empty water bowl");
        }
        System.out.println("Dog consumed water, "+waterbowl+" % left in waterbowl");
        System.out.println(" ");
    }


    public Sensor getEmptyFoodBowlSensor() {
        return emptyFoodBowlSensor;
    }

    public void setEmptyFoodBowlSensor(Sensor emptyFoodBowlSensor) {
        this.emptyFoodBowlSensor = emptyFoodBowlSensor;
    }

    public Sensor getEmptyWaterBowlSensor() {
        return emptyWaterBowlSensor;
    }

    public void setEmptyWaterBowlSensor(Sensor emptyWaterBowlSensor) {
        this.emptyWaterBowlSensor = emptyWaterBowlSensor;
    }

    public double getFoodbowl() {
        return foodbowl;
    }

    public void setFoodbowl(double foodbowl) {
        this.foodbowl = foodbowl;
    }

    public double getWaterbowl() {
        return waterbowl;
    }

    public void setWaterbowl(double waterbowl) {
        this.waterbowl = waterbowl;
    }

    @Override
    public void handleEvent(Event event){
        System.out.println("Pet cant handle events");
    }
    @Override
    public void handleAllPossibleEvents(){
        System.out.println("Pet cant handle events");
    }

}
