package cvut.fel.cz.actors;

import cvut.fel.cz.actions.Event;
import cvut.fel.cz.house.Room;

import java.util.ArrayList;

public class Child extends Person{

    public Child(ArrayList<? extends EventHandler> subscribers, int age, String name) {
        super("Child", subscribers, age, name);
    }
    @Override
    public void handleEvent(Event event){
        Event.removeFromUnhandledEvents(event);
        if(event.getEventText().equals("Empty food bowl")){
            for(Pet pet: getHouse().getPets()){
                pet.setFoodbowl(100);
                pet.getEmptyFoodBowlSensor().turnOff();
                System.out.println(this.name+" filled food bowls for "+pet.getName());
                pet.getEmptyFoodBowlSensor().notifySubscribers("food was filled");
            }
        }
        if(event.getEventText().equals("Empty water bowl")){
            for(Pet pet: getHouse().getPets()){
                pet.setWaterbowl(100);
                pet.getEmptyWaterBowlSensor().turnOff();
                System.out.println(this.name+" filled water bowls for "+pet.getName());
                pet.getEmptyFoodBowlSensor().notifySubscribers("water was filled");
            }
        }
        System.out.println("Event "+event.getEventText()+" was solved by: "+this.getId());
    }
}
