package cvut.fel.cz.actors;

import cvut.fel.cz.actions.Event;
import cvut.fel.cz.house.House;
import cvut.fel.cz.house.Room;

import java.util.List;
import java.util.UUID;

/**
 * Abstraktní třída {@code LivingBeing} představuje živou bytost.
 * Rozšiřuje třídu {@link EventHandler} a poskytuje společné vlastnosti pro všechny živé bytosti.
 */
public abstract class LivingBeing extends EventHandler {
    /**
     * Místnost, ve které se živý objekt nachází.
     */
    protected Room occupiingRoom;

    /**
     * Věk živého objektu.
     */
    protected int age;

    /**
     * Jméno živého objektu.
     */
    protected String name;

    /**
     * Stav živého objektu (např., "Home" nebo "Away").
     */
    private String status;

    /**
     * Odkaz na domovský objekt typu {@link House}, ve kterém se živý objekt nachází.
     */
    private House house;

    /**
     * Konstruktor pro vytvoření instance živého objektu se zadanými vlastnostmi.
     *
     * @param type       Typ živého objektu.
     * @param subscribers Seznam objektů odběratelů.
     * @param age        Věk živého objektu.
     * @param name       Jméno živého objektu.
     */
    public LivingBeing(String type, List<? extends EventHandler> subscribers, int age, String name) {
        super(type, (List<EventHandler>) subscribers);
        this.age = age;
        this.name = name;
        this.status = "Home";
    }




    /**
     * Vrátí odkaz na místnost, ve které se živý objekt nachází.
     *
     * @return Místnost, ve které se živý objekt nachází.
     */
    public Room getOccupiingRoom() {
        return occupiingRoom;
    }

    /**
     * Vrátí jméno živého objektu.
     *
     * @return Jméno živého objektu.
     */
    public String getName() {
        return name;
    }

    /**
     * Opustí domov.
     */
    public void leaveHouse() {
        this.occupiingRoom = null;
        System.out.println(this.getName() + " left house");
        System.out.println(" ");
    }

    /**
     * Vrátí se do první místnosti domu.
     */
    public void returnToHouse() {
        this.occupiingRoom = House.getEntrance();
        this.setStatus("Home");
        System.out.println(this.getName() + " returned home");
        System.out.println(" ");
    }





    /**
     * Vstoupí do zadané místnosti, pokud je v aktuálním stavu "Home".
     *
     * @param room Místnost, do které má vstoupit.
     */
    public void enterRoom(Room room){
        if (this.status.equals("Home")){
            this.occupiingRoom=room;
            System.out.println(this.getName()+" went to "+occupiingRoom.getNameOfRoom());
            System.out.println(" ");
        }
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public House getHouse() {
        return house;
    }

    public void setHouse(House house) {
        this.house = house;
    }
}
