
package cvut.fel.cz.actions;

import cvut.fel.cz.actors.EventHandler;
import cvut.fel.cz.documentation.AbstractReport;
import cvut.fel.cz.documentation.ActivityAndUsageReport;
import cvut.fel.cz.documentation.ConsumptionReport;
import cvut.fel.cz.documentation.EventReport;
import cvut.fel.cz.devices.ElectricDevice;

import java.time.LocalTime;
import java.util.Objects;


/**
 * Třída {@code ReportBuilder} poskytuje metody pro vytváření různých druhů zpráv.
 */
public class ReportBuilder {


    /**
     * Aktuálně vytvářená zprává.
     */
    static AbstractReport report;

    /**
     * Vytvoří zprávu o události.
     *
     * @param event Událost, na základě které se má vytvořit zpráva.
     * @return Vytvořená zpráva.
     */
    public static AbstractReport buildReport(Event event) {
        report = new EventReport();
        report.acquireDates();
        report.acquireData(event.getEventText(), event.getCaller(), event.getHandler());
        report.createReportText();
        return report;
    }


    /**
     * Vytvoří o použití zařízení.
     *
     * @param usedDevice Použité zařízení, na základě kterého se má vytvořit zpráva.
     * @return Vytvořená zpráva.
     */
    public static AbstractReport buildReport(ElectricDevice usedDevice) {
        report = new ActivityAndUsageReport(usedDevice);
        report.acquireDates();
        report.acquireData();
        report.createReportText();
        return report;
    }

    /**
     * Vytvoří zprávu o spotřebě za určité.
     *
     * @param type Typ zprávy (např., "Consumption").
     * @return Vytvořená zpráva nebo {@code null}, pokud typ není podporován.
     */
    public static AbstractReport buildReport(String type) {
        if (Objects.equals(type, "Consumption")) {
            report = new ConsumptionReport();
            report.acquireData();
            report.acquireDates();
            report.createReportText();
            return report;
        } else {
            return null;
        }
    }

}
