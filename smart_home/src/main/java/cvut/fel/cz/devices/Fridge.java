package cvut.fel.cz.devices;

import cvut.fel.cz.actions.Event;
import cvut.fel.cz.actions.ReportBuilder;
import cvut.fel.cz.actors.EventHandler;
import cvut.fel.cz.actors.Person;
import cvut.fel.cz.house.HouseAssistent;
import cvut.fel.cz.house.Room;

import java.io.FileNotFoundException;
import java.time.LocalTime;
import java.util.List;
import java.util.Locale;
import java.util.Random;

public class Fridge extends ElectricDevice {
    public Fridge(List<? extends EventHandler> subscribers, Room occupiedRoom) throws Exception {
        super("Fridge", subscribers,occupiedRoom);
        this.activeElectricityConsumption=0.02;
        this.activeWaterConsumption=0;
        this.activeGasConsumption=0;
    }
}
