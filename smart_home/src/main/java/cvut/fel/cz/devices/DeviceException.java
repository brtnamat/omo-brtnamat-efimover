package cvut.fel.cz.devices;

public class DeviceException extends Exception{
    public DeviceException(){
        super();
    }

    public DeviceException(String text){
        super(text);
    }
}
