package cvut.fel.cz.devices;

public class Heater {
    private double wantedTemperature;


    private double actualTemperature;
    private boolean turnedOn;

    public Heater(double wantedTemperature) {
        this.wantedTemperature = wantedTemperature;
        this.actualTemperature=wantedTemperature;
    }



    public void turnOn(){
        this.turnedOn=true;
    }


    public void turnOf() {
        this.turnedOn=false;
    }
    public double getWantedTemperature() {
        return wantedTemperature;
    }

    public void setWantedTemperature(double wantedTemperature) {
        this.wantedTemperature = wantedTemperature;
    }

    public double getActualTemperature() {
        return actualTemperature;
    }

    public void setActualTemperature(double actualTemperature) {
        this.actualTemperature = actualTemperature;
    }

    public boolean isTurnedOn() {
        return turnedOn;
    }
}
