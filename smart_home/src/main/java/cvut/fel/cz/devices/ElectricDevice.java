package cvut.fel.cz.devices;

import cvut.fel.cz.actions.ReportBuilder;
import cvut.fel.cz.actors.EventHandler;
import cvut.fel.cz.actors.Person;
import cvut.fel.cz.documentation.Documentation;
import cvut.fel.cz.house.Bills;
import cvut.fel.cz.house.HouseAssistent;
import cvut.fel.cz.house.Room;

import java.io.FileNotFoundException;
import java.time.Duration;
import java.time.LocalTime;
import java.util.List;
import java.util.Locale;

/**
 * Abstraktní třída {@code ElectricDevice} představuje elektrické zařízení v simulaci domácnosti.
 * Tato třída je odvozena od třídy {@code EventHandler}.
 */
public abstract class ElectricDevice extends EventHandler {

    /**
     * Místnost, ve které se elektrické zařízení nachází.
     */
    protected Room occupiedRoom;

    /**
     * Stav běhu elektrického zařízení.
     */
    protected DeviceRunningState state;

    /**
     * Osoba používající elektrické zařízení.
     */
    protected Person user;

    /**
     * Dokumentace k elektrickému zařízení.
     */
    protected Documentation documentation;

    /**
     * Aktivní spotřeba elektřiny (kW/h).
     */
    protected double activeElectricityConsumption;

    /**
     * Aktivní spotřeba vody (l/h).
     */
    protected double activeWaterConsumption;

    /**
     * Aktivní spotřeba plynu (m³/h).
     */
    protected double activeGasConsumption;

    /**
     * Celková spotřeba elektřiny (kW).
     */
    protected double electricityConsumption;

    /**
     * Celková spotřeba vody (l).
     */
    protected double waterConsumption;

    /**
     * Celková spotřeba plynu (m³).
     */
    protected double gasConsumption;

    /**
     * Životnost elektrického zařízení (0-100).
     */
    protected double durability;

    /**
     * Čas spuštění elektrického zařízení.
     */
    protected LocalTime startTime;



    /**
     * Konstruktor třídy {@code ElectricDevice}.
     *
     * @param type         Typ elektrického zařízení.
     * @param subscribers  Seznam odběratelů událostí.
     * @param occupiedRoom Místnost, ve které se zařízení nachází.
     * @throws Exception Pokud dojde k chybě při vytváření elektrického zařízení.
     */
    public ElectricDevice(String type, List<? extends EventHandler> subscribers,Room occupiedRoom) throws Exception {
        super(type, (List<EventHandler>) subscribers);
        this.durability=100.0;
        this.state=DeviceRunningState.Idle;
        this.documentation=new Documentation(type);
        this.occupiedRoom=occupiedRoom;
    }

    /**
     * Vypne elektrické zařízení.
     */
    public void turnOff(){
        this.electricityConsumption=0.0;
        this.waterConsumption=0.0;
        this.gasConsumption=0.0;
        this.state=DeviceRunningState.Off;
        System.out.println(this.type+" was turned off");
    }


    /**
     * Zapne elektrické zařízení.
     *
     * @throws DeviceException Pokud dojde k chybě při zapínání zařízení.
     */
    public void turnOn() throws DeviceException {
        if (this.state==DeviceRunningState.Broken) {
            throw new DeviceException();
        }
        this.state = DeviceRunningState.Idle;
        System.out.println(this.type+" was turned on");
    }


    /**
     * Začne používat elektrické zařízení.
     *
     * @param user Osoba, která bude elektrické zařízení používat.
     * @throws DeviceException Pokud dojde k chybě při používání zařízení.
     */
    public void use(Person user) throws DeviceException{
        if(this.state==DeviceRunningState.Off){
            this.turnOn();
        } else if (this.state==DeviceRunningState.Broken){
            System.out.println("Person is trying to use broken " + this.type);
            return;
        }
        this.startTime=HouseAssistent.getTime();
        this.state=DeviceRunningState.Active;
        this.user=user;
        this.electricityConsumption=activeElectricityConsumption;
        this.waterConsumption=activeWaterConsumption;
        this.gasConsumption=activeGasConsumption;
        this.durability=this.durability-20;
    }



    /**
     * Přestane používat elektrické zařízení.
     */
    public void stopUsing(){

        if(this.durability<=0){
            this.notifySubscribers("broke");
            this.state=DeviceRunningState.Broken;
        }else{
            this.state=DeviceRunningState.Idle;
        }
            Duration duration = Duration.between(startTime, HouseAssistent.getTime());
            double hoursRoundedUp = Math.ceil((duration.toMinutes()+1) / 60.0);
            Bills.addToUsedElectricity(electricityConsumption*hoursRoundedUp);
            Bills.addToUsedWater(waterConsumption* hoursRoundedUp);
            Bills.addToUsedGas(gasConsumption* hoursRoundedUp);
            this.gasConsumption=0;
            this.electricityConsumption=0;
            this.waterConsumption=0;
            ReportBuilder.buildReport(this);
            this.startTime=null;

        this.user=null;
    }





    public Double getElectricityConsumption() {
        return electricityConsumption;
    }

    public Double getWaterConsumption() {
        return waterConsumption;
    }

    public Double getGasConsumption() {
        return gasConsumption;
    }

    public Person getUser() {
        return user;
    }

    public Double getDurability() {
        return durability;
    }

    @Override
    public String getType(){
        return this.type;
    }

    public Room getOccupiedRoom() {
        return occupiedRoom;
    }

    public void setOccupiedRoom(Room occupiedRoom) {
        this.occupiedRoom = occupiedRoom;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public void setDurability(double durability) {
        this.durability = durability;
    }

    public Documentation getDocumentation() {
        return documentation;
    }

    public void setState(DeviceRunningState state) {
        this.state = state;
    }

    public DeviceRunningState getState() {
        return state;
    }
}
