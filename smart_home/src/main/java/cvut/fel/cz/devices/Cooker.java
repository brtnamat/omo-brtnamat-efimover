package cvut.fel.cz.devices;

import cvut.fel.cz.actors.EventHandler;
import cvut.fel.cz.house.Room;

import java.io.FileNotFoundException;
import java.util.List;

public class Cooker extends ElectricDevice{
    public Cooker( List<? extends EventHandler> subscribers, Room occupiedRoom) throws Exception {
        super("Cooker", subscribers, occupiedRoom);
        this.activeElectricityConsumption=0.5;
        this.activeGasConsumption=1;
        this.activeWaterConsumption=0;
    }
}
