package cvut.fel.cz.devices;

import cvut.fel.cz.actions.Event;
import cvut.fel.cz.actors.EventHandler;

import java.util.List;

public class Sensor extends EventHandler {

    private boolean turnedOn;

    public Sensor( List<EventHandler> subscribers) {
        super("Sensor", subscribers);
    }


    public void turnOn(){
        this.turnedOn=true;
    }

    public void turnOff(){
        this.turnedOn=false;
    }


}
