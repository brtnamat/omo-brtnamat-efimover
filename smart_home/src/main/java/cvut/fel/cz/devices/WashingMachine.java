package cvut.fel.cz.devices;

import cvut.fel.cz.actions.Event;
import cvut.fel.cz.actions.ReportBuilder;
import cvut.fel.cz.actors.EventHandler;
import cvut.fel.cz.actors.Person;
import cvut.fel.cz.house.Room;

import java.io.FileNotFoundException;
import java.time.LocalTime;
import java.util.List;
import java.util.Locale;
import java.util.Timer;

public class WashingMachine extends ElectricDevice {


    public WashingMachine(List<? extends EventHandler> subscribers, Room occupiedRoom) throws Exception {
        super("WashingMachine", subscribers,occupiedRoom);
        this.activeElectricityConsumption=0.05;
        this.activeGasConsumption=0.1;
        this.activeWaterConsumption=0.5;
    }

//    @Override
//    public void use(Person user) throws DeviceException{
//        if(this.state==DeviceRunningState.Off){
//            this.turnOn();
//        } else if (this.state==DeviceRunningState.Broken || this.user!=null){
//            throw new DeviceException();
//        }else if(this.state==DeviceRunningState.Active){
//            System.out.println("Pračka zrovna pere a proto nelze použít");
//            return;
//        }
//        if(weightOfClothesIn>0.5 && weightOfClothesIn<5 ){
//            System.out.println("V pračce je špatné množství oblečeni");
//            return;
//        }
//        this.state=DeviceRunningState.Active;
//        this.user=user;
//        this.electricityConsumption=activeElectricityConsumption;
//        this.waterConsumption=activeWaterConsumption;
//        this.durability=this.durability-0.4;
//    };


//    public void stopUsing(){
//        this.user=null;
//        if(this.durability<=0){
//            this.notifySubscribers("broke");
//            this.state=DeviceRunningState.Broken;
//        }else{
//            this.state=DeviceRunningState.Idle;
//        }
//        this.stopTime=stopTime;
//        ReportBuilder.buildReport(this);
//    }
}
