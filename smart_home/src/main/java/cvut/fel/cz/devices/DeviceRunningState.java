package cvut.fel.cz.devices;
/**
 * Enumerace {@code DeviceRunningState} představuje stav běhu elektrického zařízení.
 * Možné stavy zahrnují: Aktivní, Nečinné, Vypnuto, Rozbito.
 */
public enum DeviceRunningState {
    /**
     * Stav, kdy je elektrické zařízení aktivní a provádí svou funkci.
     */
    Active,

    /**
     * Stav, kdy je elektrické zařízení nečinné a není v provozu.
     */
    Idle,

    /**
     * Stav, kdy je elektrické zařízení vypnuto.
     */
    Off,

    /**
     * Stav, kdy je elektrické zařízení rozbito a není funkční.
     */
    Broken

}
