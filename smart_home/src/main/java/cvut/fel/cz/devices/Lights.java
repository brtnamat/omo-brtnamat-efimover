package cvut.fel.cz.devices;

import cvut.fel.cz.actors.EventHandler;

import java.util.List;

public class Lights  {


    private boolean turndedOn;
    private double intensity;
    public Lights() {
        this.turndedOn=false;
        this.intensity=0;
    }


    public void turnOn(){
        this.turndedOn=true;
        this.intensity=70.0;
    }
    public void turnOn(double intensity){
        this.turndedOn=true;
        this.intensity=intensity;
    }
    public void turnOf(){
        this.turndedOn=false;
        this.intensity=0;
    }

    public boolean isTurndedOn() {
        return turndedOn;
    }

    public double getIntensity() {
        return intensity;
    }
}
