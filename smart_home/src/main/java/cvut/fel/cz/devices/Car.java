package cvut.fel.cz.devices;

import cvut.fel.cz.actors.Adult;
import cvut.fel.cz.actors.EventHandler;

import java.util.ArrayList;
import java.util.List;
/**
 * Třída {@code Car} reprezentuje objekt automobilu.
 */

public class Car extends EventHandler {

    /**
     * Příznak, zda je automobil zamčený.
     */
    private boolean locked;

    /**
     * Příznak, zda je automobil zapnutý.
     */
    private boolean running;

    /**
     * Uživatel automobilu (dospělý).
     */
    private Adult user;

    /**
     * Množství paliva v nádrži.
     */
    private double tank;

    /**
     * Příznak, zda je automobil v provozu.
     */
    private boolean inUse;

    /**
     * Senzor pro detekci prázdné nádrže.
     */
    private final Sensor emptyTankSensor;

    /**
     * Spotřeba paliva automobilu ve formě litrů na 100 km.
     */
    private final double fuelConsumption; // l/100 kmh

    /**
     * Konstruktor třídy {@code Car}.
     *
     * @param subscribers Seznam objektů, které mají být informovány o událostech automobilu.
     */
    public Car(List<? extends EventHandler> subscribers) {
        super("Car", (List<EventHandler>) subscribers);
        this.fuelConsumption = 8;
        this.tank = 55;
        this.emptyTankSensor = new Sensor(null);
    }




    /**
     * Odemkne automobil.
     */
    public void unlock() {
        this.locked = false;
    }

    /**
     * Zamkne automobil.
     */
    public void lock() {
        this.locked = true;
    }

    /**
     * Spustí automobil.
     */
    public void start() {
        notifySubscribers("Car started");
        this.running = true;
    }



    /**
     * Zastaví automobil.
     */
    public void stop() {
        notifySubscribers("Car stopped");
        this.running = false;
        this.user = null;
    }

    /**
     * Naplní nádrž automobilu o zadaný počet litrů paliva.
     *
     * @param liters Množství litrů paliva k naplnění.
     */
    public void fillTheTank(double liters) {
        if (this.tank + liters <= 55) {
            this.tank = this.tank + liters;
        } else {
            this.tank = 55;
        }
    }


    /**
     * Naplní nádrž automobilu na maximální kapacitu.
     */
    public void  fillTheTank(){
        this.tank=55;
    }

    /**
     * Řídí automobil s uživatelem na zadanou vzdálenost v kilometrech.
     *
     * @param adult Uživatel automobilu (dospělý).
     * @param kms   Vzdálenost v kilometrech.
     */
    public void drive(Adult adult, double kms) {
        this.user = adult;
        for (double i = 0; i < kms; i = i + 10) {
            this.tank = this.tank - this.fuelConsumption * ((double) 10 / 100);
            if (this.tank <= 10) {
                adult.getNotification("Car will need refueling", this);
                this.emptyTankSensor.turnOn();
            }
        }
    }

    public double getTank() {
        return tank;
    }

    public Sensor getEmptyTankSensor() {
        return emptyTankSensor;
    }

    public void setTank(double tank) {
        this.tank = tank;
    }

    public boolean isInUse() {
        return inUse;
    }

    public void setInUse(boolean inUse) {
        this.inUse = inUse;
    }

    public Adult getUser() {
        return user;
    }
}
