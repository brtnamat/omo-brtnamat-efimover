package cvut.fel.cz.devices;

import cvut.fel.cz.actors.EventHandler;
import cvut.fel.cz.house.Room;

import java.io.FileNotFoundException;
import java.util.List;

public class Computer extends ElectricDevice{

    public Computer(List<? extends EventHandler> subscribers, Room occupiedRoom) throws Exception {
        super("Computer", subscribers,occupiedRoom);
        this.activeElectricityConsumption=0.26;
        this.activeWaterConsumption=0;
        this.activeGasConsumption=0;
    }
}
