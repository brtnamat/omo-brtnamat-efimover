package cvut.fel.cz;

import cvut.fel.cz.devices.DeviceException;
import cvut.fel.cz.house.HouseAssistent;

import java.time.LocalDateTime;
import java.util.Scanner;


public class Main {
    public static void main(String[] args) throws Exception {
        Scanner scanner=new Scanner(System.in);
        System.out.println("Napiště číslo konfigurace kterou chcete použít");
        String configNumber=scanner.nextLine();
        int intValue=1;
        try {
           intValue = Integer.parseInt(configNumber);
        } catch (NumberFormatException e) {
            System.err.println("Tento vstup není číslo, je potřeba zadat číslo, bude použit defaultní config ");
        }
        Simulation.startSimulation(intValue);

    }
}