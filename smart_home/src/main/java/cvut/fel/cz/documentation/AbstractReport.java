package cvut.fel.cz.documentation;

import cvut.fel.cz.actors.EventHandler;

import java.io.*;
import java.util.Date;

/**
 * Abstraktní třída {@code AbstractReport} slouží jako základ pro vytváření textových reportů
 * a implementuje rozhraní {@code ReportInterface}.
 */
public abstract class AbstractReport implements ReportInterface {

    /**
     * Textová data pro report.
     */
    protected String reportData;

    /**
     * Typ reportu.
     */
    protected String type;

    /**
     * Konstruktor třídy {@code AbstractReport}.
     *
     * @param type Typ reportu.
     */
    public AbstractReport(String type) {
        this.type = type;
    }

    /**
     * Získává data pro report na základě události, volajícího a zpracovatele.
     *
     * @param event  Událost, ke které jsou data přidávána.
     * @param caller Volající objekt události.
     * @param handler Zpracovatel události.
     */
    public void acquireData(String event, EventHandler caller, EventHandler handler) {
    }

    /**
     * Získává obecná data pro report.
     */
    public void acquireData() {
    }

    /**
     * Získává data o dnech a časech pro report.
     */
    public void acquireDates() {
    }

    /**
     * Vytváří textový obsah reportu.
     */
    public void createReportText() {
    }
    public String getReportData(){
        return this.reportData;
    }
    /**
     * Zapíše report do souboru.
     */
    public void writeToFile(){
        String filename=this.type+"Report.txt";
        File file=new File(filename);
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file,true))) {
            writer.write(reportData);
            writer.newLine();
            System.out.println("Writen to file: "+"\n"+this.reportData);
            System.out.println("\n");
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
}
