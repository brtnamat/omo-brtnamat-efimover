package cvut.fel.cz.documentation;

import cvut.fel.cz.house.Bills;
import cvut.fel.cz.house.HouseAssistent;

import java.util.Date;

public class ConsumptionReport extends AbstractReport{
    private Double finalCost;
    private Double totalElectricityCost;
    private Double totalWaterCost;
    private Double totalGasCost;
    private String fromToDate;

    public ConsumptionReport() {
        super("Consumption");
    }


    @Override
    public void acquireData() {
        this.totalGasCost= Bills.getGasCost()*Bills.getUsedGas();
        this.totalWaterCost=Bills.getWaterCost()* Bills.getUsedWater();
        this.totalElectricityCost=Bills.getElectricityCost()*Bills.getUsedElectricity();
        this.finalCost=this.totalElectricityCost+this.totalGasCost+this.totalWaterCost;
    }

    @Override
    public void acquireDates() {
        StringBuilder str=new StringBuilder();
        str.append("From: ").append(Bills.getFromDate());
        str.append(" To: ").append(HouseAssistent.getDate());
        this.fromToDate=str.toString();
    }

    @Override
    public void createReportText() {
        StringBuilder str=new StringBuilder();
        str.append(fromToDate).append(" ");
        str.append("\n");
        str.append("Gas cost for last session was: ").append(totalGasCost).append(" eur");
        str.append("\n");
        str.append("Electricity cost for last session was: ").append(Math.abs(totalElectricityCost)).append(" eur");
        str.append("\n");
        str.append("Water cost for last session was: ").append(Math.abs(totalWaterCost)).append(" eur");
        str.append("\n");
        str.append("Total cost for last session was: ").append(Math.abs(finalCost)).append(" eur");
        this.reportData=str.toString();
        System.out.println("Consumption report generated");
        this.writeToFile();

    }

    
}
