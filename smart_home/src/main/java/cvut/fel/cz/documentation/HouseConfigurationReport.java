package cvut.fel.cz.documentation;

import cvut.fel.cz.actors.Adult;
import cvut.fel.cz.actors.Child;
import cvut.fel.cz.actors.Person;
import cvut.fel.cz.actors.Pet;
import cvut.fel.cz.devices.DeviceException;
import cvut.fel.cz.devices.ElectricDevice;
import cvut.fel.cz.house.Floor;
import cvut.fel.cz.house.House;
import cvut.fel.cz.house.Room;
import cvut.fel.cz.house.Window;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class HouseConfigurationReport extends AbstractReport {

    private int numOfFloors;
    private int numOfRooms;
    private int numOfWindows;
    private int numOfOccupants;
    private int numOfPets;

//    private List<ElectricDevice> devices;

    private  String petsNames;
    private String occupantsNames;
    private final House house;


    public HouseConfigurationReport(House house, String str) {
        super(str);
        this.house=house;
//        this.devices=new ArrayList<>();
//        this.devices.add(house.getDevices().get(0));
    }

    @Override
    public void acquireData() {
        List<Floor> floors = this.house.getFloors().stream()
                .toList();

        List<Room> rooms=floors.stream()
                .flatMap(floor -> floor.getRooms().stream())
                .toList();

        List<Window> windows = rooms.stream()
                .flatMap(room -> room.getWindows().stream())
                .toList();

//        this.devices=this.house.getDevices();

        this.numOfRooms=rooms.size();
        this.numOfFloors=floors.size();
        this.numOfWindows=windows.size();



        StringBuilder str=new StringBuilder("People living in the house:");
        this.numOfOccupants=0;
        for(Adult person:house.getAdults()){
            this.numOfOccupants++;
            str.append(" ").append(person.getName()).append(" with id: ").append(person.getId()).append("\n");
        }
        for(Child person:house.getChildren()){
            this.numOfOccupants++;
            str.append(" ").append(person.getName()).append(" with id: ").append(person.getId()).append("\n");
        }

        this.occupantsNames=str.toString();


        str=new StringBuilder("Pets living in the house:");
        this.numOfPets=0;
        for(Pet pet:house.getPets()){
            this.numOfPets++;
            str.append(" ").append(pet.getName());
        }
        this.petsNames=str.toString();

    }


    @Override
    public void createReportText() {
        StringBuilder str=new StringBuilder();
        str.append("Number of floors in the house: ").append(numOfFloors).append('\n');
        str.append("Number of rooms in the house: ").append(numOfRooms).append('\n');
        str.append("Number of windows in the house: ").append(numOfWindows).append('\n');
        str.append('\n');
        str.append("Number of family members living in the house: ").append(numOfOccupants).append('\n');
        str.append(occupantsNames).append('\n');
        str.append("Number of pets living in the house: ").append(numOfPets).append('\n');
        str.append(petsNames).append('\n');
//        str.append("Number of devices in the house: ").append(devices.size()).append('\n');
//        str.append('\n');
//        for (ElectricDevice device:devices){
//            str.append(device.getType()).append(" with id:").append(device.getId()).
//                    append(" is placed at: ").append(device.getOccupiedRoom().getNameOfRoom()).append('\n');
//        }
        this.reportData=str.toString();
        System.out.println("House configuration report generated.");
        System.out.println(' ');
        this.writeToFile();
    }
}
