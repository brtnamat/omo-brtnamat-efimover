package cvut.fel.cz.documentation;

import cvut.fel.cz.actors.EventHandler;

public interface ReportInterface {
    public void acquireData();
    public void acquireData(String event, EventHandler caller, EventHandler handler);

    public void acquireDates();

    public void createReportText();

}
