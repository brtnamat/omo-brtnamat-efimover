package cvut.fel.cz.documentation;

import cvut.fel.cz.actors.Adult;

import java.awt.image.AreaAveragingScaleFilter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;

public class Documentation {

    private ArrayList<Adult> readByAdults;
    private String text;

    public Documentation(String type) throws Exception {
        this.readByAdults = new ArrayList<>();
        try {
            this.text= new FileReader(type+"Documentation.txt").toString();
        }catch (Exception e){
            throw new Exception("Required document does not exists");
        }

    }

    public String getText() {
        return text;
    }

    public void read(Adult adult){
        if (this.readByAdults.contains(adult)){
            System.out.println(adult.getName()+" already knows manual");
        }
        readByAdults.add(adult);;
        System.out.println(adult.getName()+" reads manual");


    }


}
