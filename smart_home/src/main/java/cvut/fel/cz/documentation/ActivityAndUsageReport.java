package cvut.fel.cz.documentation;

import cvut.fel.cz.actors.Person;
import cvut.fel.cz.devices.ElectricDevice;
import cvut.fel.cz.house.HouseAssistent;

import java.time.LocalDate;
import java.time.LocalTime;

public class ActivityAndUsageReport extends AbstractReport {
    private ElectricDevice usedDevice;
    private Double leftDurability;
    private LocalDate usedDate;
    private LocalTime finishTime;
    private  LocalTime startTime;
    private Person user;

    public ActivityAndUsageReport(ElectricDevice usedDevice) {
        super("Activity");
        this.usedDevice=usedDevice;
    }

    @Override
    public void acquireData() {
        this.user=this.usedDevice.getUser();
        this.leftDurability=this.usedDevice.getDurability();
    }

    @Override
    public void acquireDates() {
        this.usedDate= HouseAssistent.getDate();
        this.startTime=this.usedDevice.getStartTime();
        this.finishTime =HouseAssistent.getTime();
    }

    @Override
    public void createReportText() {
        StringBuilder str=new StringBuilder();
        str.append("Device: ").append(usedDevice.getType());
        str.append(" was used by: ").append(this.user.getName());
        str.append(" on date: ").append(this.usedDate);
        str.append("  started at ").append(this.startTime).append(".");
        str.append("  stopped at ").append(this.finishTime).append(".");
        str.append(" Durability left: ").append(this.leftDurability);
        this.reportData=str.toString();
        this.writeToFile();
    }
}
