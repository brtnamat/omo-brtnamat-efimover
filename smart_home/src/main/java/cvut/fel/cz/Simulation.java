package cvut.fel.cz;

import cvut.fel.cz.actions.Event;
import cvut.fel.cz.actions.ReportBuilder;
import cvut.fel.cz.actors.*;
import cvut.fel.cz.devices.*;
import cvut.fel.cz.house.*;

import java.io.*;
import java.util.*;


public class Simulation {

    /**
     * Instance třídy {@code Random} pro generování náhodných hodnot.
     */
    public static final Random random = new Random();

    private static House house;


    /**
     *  Pole typů místností které se cyklicky opakují.
     */
    private static final String[] roomChronology={"Kitchen","Bedroom","Bathroom","LivingRoom","Bedroom"};

    /**
     *  Pole jmen mužů, které se cyklicky opakují.
     */
    private static final String[] maleNames={"Petr","Jonas","Jan","Tomas","Jiri","Filip","Vojta","Martin"};

    /**
     *  Pole jmen žen, které se cyklicky opakují.
     */
    private static final String[] femaleNames={"Johana","Veronika","Eliska","Natalie","Viktorie","Marketa","Ivana","Eva"};

    /**
     *  Pole jmen dětí,které se cyklicky opakují.
     */
    private  static final String[] childrenNames={"Petrik","Anicka","Honza","Johanka","Mata","Viktorka"};

    /**
     *  Pole jemn mazlíčků,které se cyklicky opakují.
     */
    private static final String[] petsNames={"Azor","Bambi","Fluffy","Tweety","Daisy"};




    /**
     * Spustí simulaci s určeným konfiguračním číslem.
     *
     * @param confNumber Číslo konfigurace pro načtení konfiguračních informací.
     * @throws Exception Pokud dojde k chybě při provádění simulace.
     */
    public static void startSimulation(int confNumber) throws Exception {
        house = configure(confNumber);
        boolean running=true;
        Scanner scanner=new Scanner(System.in);
        HouseAssistent houseAssistent = HouseAssistent.getInstance(house);
        while (running) {
            for (int i = 0; i < 8; i++) {
                houseAssistent.addHalfHour();
                houseAssistent.showInfo();
                int actionCounter = random.nextInt(5);
                for (int j = 0; j < actionCounter; j++) {
                    generatePersonAction();
                }
                if (i % 6 == 0) {
                    petDrink();
                }
                if (i % 8 == 0) {
                    petEat();
                    if (!Event.getUnhandledEvents().isEmpty()) {
                        for (Adult adult : house.getAdults()) {
                            if (adult.getStatus().equals("Home")) {
                                adult.handleAllPossibleEvents();
                            }
                        }
                        for (Child child : house.getChildren()) {
                            if (child.getStatus().equals("Home")) {
                                child.handleAllPossibleEvents();
                            }
                        }
                    }
                }
                if (actionCounter < 1) {
                    useCar();
                }
            }
            System.out.println("Chcete pokračovat v simulaci?");
            String input=scanner.nextLine();
            if (input.equals("q")){
                running=false;
            }
            int rainningSwitch=random.nextInt(3);
            if(rainningSwitch==2){
                houseAssistent.switchRainning();
            }
        }
        ReportBuilder.buildReport("Consumption");
        Bills.createBillSession();
        System.out.println(HouseAssistent.getDate());
    }



    public static void useCar(){
        Adult adult=house.getAdults().get(random.nextInt(house.getAdults().size()));
        if(Objects.equals(adult.getStatus(), "Home") && !house.getCar().isInUse()){
            adult.driveCar(house.getCar(),random.nextDouble(100)+5);
        } else if (house.getCar().isInUse()) {
            house.getCar().getUser().returnWithCar(house.getCar());

        }
    }


    /**
     * Metoda simulující jezení mazlíčků
     */
    public static void petEat(){
        Pet pet=house.getPets().get(random.nextInt(house.getPets().size()));
        pet.eat();
    }


    /**
     * Metoda simulující pití mazlíčků
     */
    public static void petDrink(){
        Pet pet=house.getPets().get(random.nextInt(house.getPets().size()));
        pet.drink();
    }

    /**
     * Generuje náhodné akce pro obyvatele domu pokud není noc, v tu chvíli všichni spí.
     *
     * @throws DeviceException Pokud dojde k chybě při manipulaci s elektrickými zařízeními.
     */
    public static void generatePersonAction() throws DeviceException {
        if (HouseAssistent.getTime().isBefore(HouseAssistent.getDayTime()) || HouseAssistent.getTime().isAfter(HouseAssistent.getNightTime())) {
            return;
        }
        int listSelector = random.nextInt(2);

        try {
            Person actor;
            if (listSelector == 0) {
                actor = house.getAdults().get(random.nextInt(house.getAdults().size()));

            } else {
                actor = house.getChildren().get(random.nextInt(house.getChildren().size()));
            }

            if (actor.getUsingDevice() != null) {
                actor.stopUsingDevice();
            } else if(Objects.equals(actor.getStatus(), "Home")) {
                ElectricDevice device = house.getDevices().get(random.nextInt(house.getDevices().size()));
                if (device.getUser() == null) {
                    actor.useDevice(device);
                } else {
                    device.getUser().stopUsingDevice();
                }

            }
        } catch (DeviceException e) {
            throw new DeviceException("Person wants to use device that is not able to be used");
        }

    }


    /**
     * Maže obsah souborů, které jsou používány pro reporty v rámci simulace.
     *
     * @throws Exception Pokud dojde k chybě při mazání souborů.
     */
    public static void eraseFiles() throws Exception {
        String[] names = {"Activity", "Consumption", "Event", "HouseConfiguration"};
        for (String type : names) {
            String filename = type + "Report.txt";
            File file = new File(filename);
            try (BufferedWriter writer = new BufferedWriter(new FileWriter(file, false))) {
                writer.write("");
            } catch (Exception ex) {
                throw new Exception(ex);
            }
        }
        System.out.println("Erased text files" + '\n');
    }


    /**
     * Konfiguruje domovskou situaci v rámci simulace.
     *
     * @param confNumber Číslo konfigurace pro načtení konfiguračních informací.
     * @return Vytvořená instance domu.
     * @throws Exception Pokud dojde k chybě při konfiguraci domu.
     */
    public static House configure(int confNumber) throws Exception {
        eraseFiles();

        Properties properties = new Properties();


        try {
            properties.load(new FileReader("config"+confNumber+".properties"));
        }catch (Exception e){
            System.err.println("Tento file neexistuje");
            System.exit(1);
        }




        int floorCount = Integer.parseInt(properties.getProperty("floorCount", "1"));
        int roomCount = Integer.parseInt(properties.getProperty("roomCount", "4"));
        int adultCount = Integer.parseInt(properties.getProperty("adultCount", "2"));
        int childCount = Integer.parseInt(properties.getProperty("childCount", "2"));
        int petCount = Integer.parseInt(properties.getProperty("petCount", "1"));


        ArrayList<Floor> floors = new ArrayList<>();

        ArrayList<Child> children = new ArrayList<>();
        ArrayList<Adult> adults = new ArrayList<>();
        ArrayList<Pet> pets = new ArrayList<>();


        for(int i=0;i<adultCount;i++){
            String[] names;
            if(i%2==0){
                names=maleNames;
            }else {
                names=femaleNames;
            }
            adults.add(new Adult(null,random.nextInt(21) + 30,names[i%names.length]));
            System.out.println(i);
        }

        for(int i=0;i<childCount;i++){
            children.add(new Child(adults,random.nextInt(7) + 10,childrenNames[i%childrenNames.length]));
        }


        for(int i=0;i<petCount;i++){
            pets.add(new Pet(children,5,petsNames[i%petsNames.length]));
        }




        for (int i = 0; i < floorCount; i++) {
            Floor floor = new Floor(i + 1);
            Room connectingRoom=new Room("Connecting room");
            floor.getRooms().add(connectingRoom);
            for(int j=0; j<roomCount;j++){
                Room room=new Room(roomChronology[j%roomChronology.length]);
                if(Objects.equals(room.getNameOfRoom(), "Kitchen")){
                    room.addToDevices(new Fridge(adults,room));
                    room.addToDevices(new Cooker(adults,room));
                } else if (Objects.equals(room.getNameOfRoom(), "Bathroom")) {
                    room.addToDevices(new WashingMachine(adults,room));
                } else if (Objects.equals(room.getNameOfRoom(),"Bedroom")) {
                    room.addToDevices(new Computer(adults,room));
                }
                floor.getRooms().add(room);
                ArrayList<Window> windows=new ArrayList<>();
                windows.add(new Window(adults));
                room.setWindows(windows);
            }
            floors.add(floor);
        }
        Room garage=new Room("Garage");
        floors.get(0).getRooms().add(garage);



        House house = House.getInstance(floors, children, adults, pets);
        Car car=new Car(adults);
        house.addCar(car);
        List<ElectricDevice> electricDevices = floors.stream()
                .flatMap(floor -> floor.getRooms().stream())
                .flatMap(room -> room.getDevices().stream())
                .toList();
        house.setDevices(electricDevices);
        House.setGarage(garage);

        for (Adult adult:adults){
            adult.setHouse(house);
            adult.enterRoom(floors.get(0).getRooms().get(roomCount-1));
        }
        for (Child child:children){
            child.setHouse(house);
            child.enterRoom(floors.get(floorCount-1).getRooms().get(roomCount-1));
        }

        return house;
    }
}
