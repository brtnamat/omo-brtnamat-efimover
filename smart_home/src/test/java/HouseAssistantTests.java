import cvut.fel.cz.Simulation;
import cvut.fel.cz.house.House;
import cvut.fel.cz.house.HouseAssistent;
import org.junit.jupiter.api.*;

import java.time.LocalDate;
import java.time.LocalTime;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class HouseAssistantTests {
    static House house;
    static HouseAssistent houseAssistent;
    @BeforeAll
    public static void setUp() throws Exception {
        house = Simulation.configure(1);
        houseAssistent = HouseAssistent.getInstance(house);
    }


    @Test
    @Order(1)
    public void cratingTime(){
        Assertions.assertEquals(LocalTime.of(7,0), HouseAssistent.getTime());
    }
    @Test
    @Order(2)
    public void addingTime(){
        houseAssistent.addHalfHour();
        Assertions.assertEquals(LocalTime.of(7,30),HouseAssistent.getTime());
    }


    @Test
    @Order(3)
    public void creatingDate(){
        Assertions.assertEquals(LocalDate.of(2024,1,1),HouseAssistent.getDate());
    }

    @Test
    @Order(4)
    public void addingDay(){
        for (int i=0;i<48;i++){
            houseAssistent.addHalfHour();
        }
        Assertions.assertEquals(LocalDate.of(2024,1,2),HouseAssistent.getDate());
    }



}
