import cvut.fel.cz.Simulation;
import cvut.fel.cz.house.House;
import cvut.fel.cz.house.HouseAssistent;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class ConfigurationTests {

    static House house;
    static HouseAssistent houseAssistent;
    @BeforeAll
    public static void setUp() throws Exception {
        house = Simulation.configure(1);
        houseAssistent = HouseAssistent.getInstance(house);
    }

    @Test
    public void adultsCreation(){
        Assertions.assertEquals(house.getAdults().size(),2);
    }
    @Test
    public void childrenCreation(){
        Assertions.assertEquals(house.getChildren().size(),2);
    }

    @Test
    public void petCreation(){
        Assertions.assertEquals(house.getPets().size(),2);
    }

    @Test
    public void entranceCreation(){
        Assertions.assertEquals(House.getEntrance(),house.getFloors().get(0).getRooms().get(0));
    }
    @Test
    public void roomsCreation(){
        Assertions.assertEquals(house.getFloors().get(0).getRooms().size(),6);
    }

}
