import cvut.fel.cz.Simulation;
import cvut.fel.cz.actors.Adult;
import cvut.fel.cz.devices.DeviceException;
import cvut.fel.cz.devices.DeviceRunningState;
import cvut.fel.cz.devices.ElectricDevice;
import cvut.fel.cz.house.House;
import cvut.fel.cz.house.HouseAssistent;
import org.junit.jupiter.api.*;


@TestMethodOrder(MethodOrderer.OrderAnnotation.class)

public class DeviceTest {
    static House house;
    static HouseAssistent houseAssistent;
    @BeforeEach
    public void setUp() throws Exception {
        house = Simulation.configure(1);
        houseAssistent = HouseAssistent.getInstance(house);
    }

    @Test
    public void durabilityTest() throws DeviceException {
        Adult adult=house.getAdults().get(0);
        ElectricDevice device=house.getDevices().get(0);
        adult.useDevice(device);
        adult.stopUsingDevice();
        Assertions.assertEquals(device.getDurability(),80);

    }
    @Test
    public void durabilityTest_doubleUse_80() throws DeviceException {
        Adult adult=house.getAdults().get(0);
        ElectricDevice device=house.getDevices().get(0);
        adult.useDevice(device);
        adult.useDevice(device);
        adult.stopUsingDevice();
        Assertions.assertEquals(device.getDurability(),80);
    }
    @Test
    public void durabilityTest_doubleUse_20() throws DeviceException {
        Adult adult=house.getAdults().get(0);
        ElectricDevice device=house.getDevices().get(0);
        for(int i=0;i<4;i++){
            adult.useDevice(device);
            adult.stopUsingDevice();
        }
        Assertions.assertEquals(device.getDurability(),20);
    }

    @Test
    public void userTest_Adult() throws DeviceException {
        ElectricDevice device=house.getDevices().get(0);
        Adult adult=house.getAdults().get(0);
        adult.useDevice(device);
        Assertions.assertEquals(device.getUser(),adult);
    }

    @Test
    public void destroyed_Device() throws DeviceException {
        ElectricDevice device=house.getDevices().get(0);
        Adult adult=house.getAdults().get(0);
        for(int i=0;i<5;i++){
            adult.useDevice(device);
            adult.stopUsingDevice();
        }
        Assertions.assertEquals(DeviceRunningState.Broken,device.getState());
    }
}
