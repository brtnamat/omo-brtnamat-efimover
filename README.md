# OMO - brtnamat-efimover


## Popis

Projekt na předmět "Objektové modelování". Jedná se o simulaci smart house domova. Simulace obsahuje třídy představující části domů,  ruzné zařízení které se v domě nachází a obyvatele domu kteří zde žijí. Základem simulace jsou tzv. "reporty", které jsou generovány. Jedná se o textové soubory, které jsou generovány na základě událostí, které se v simulaci odehráli.
Tyto reporty jsou skládány třídou ReportBuilder.


## Spuštění

1. Po spuštění bude dotazán, který config file si přejete vybrat.
2. V základě existují 2 config fily (1 a 2).
3. Při spuštění se špatným argumentem bude vybrán config file číslo jedna, pokud to bude možné.
4. Pokud se program spustí, provádí se nekonečně cykly dokud uživatel nezmáčkne po cyklu "q".


## Ovládání

Program v simulaci, funguje na základě přidávání času po půl hodině.
V každém cyklu simulace bude  k času postupně několikrát přidána půhodina a vygenerováno několik akcí. Po průběhu každého cyklu bude uživatel dotázán jestli chce pokračovcat v simulaci. Když chcete ukončit simulaci, napiště "q" a zmáčkněte enter. Na konci simulace bude vytvořen poslední "ConsumptionReport" a program se vypne.


## Testování

Program je testován unit testy. Testována je především třída ElectricDevice, kde je testováno, jestli spravně funguje metoda use() a atribut durability, který reprezentuje poškozování zařízení při jeho používání.


## Design paterny

1. Builder - využit v ReportBuilder - má za úkol generovat nové reporty a zapisovat je do textových souborů.
2. Singelton - využit v HouseAssistant a House - tento design pattern zde byl použit aby se předešlo dvojímu vytvoření instancí tříd, které se mají v programu vytvořit pouze jednou.
3. Factory method - využit v EventHandleru 
4. Subscriber/Publish - využit v EventHandleru - každý EventHandler může odebírat jiného EventHandlera, který informuje o určitých událostech všechny svoje subscribery.
5. Private Class data - využit v ElectricDevice - zde je documentace uložena do vlastní třídy, která si drží svůj text manuálu a list lidí kteří si ho přečetli.


## Documentace

Documentace je předložena ve formě java doc komentářů hlavních tříd a nimi vytvořeným java doc dokumentem s názvem "index.html".


## Diagramy

Domain object modely jsou přiloženy ve dvou formách (plánovaný a skutečný). V souborech s diagramy se také nachází Use case diagramy.


